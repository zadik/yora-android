package carlos_0416.yora.services;

import com.squareup.otto.Subscribe;

import carlos_0416.yora.yora.infrastructure.Auth;
import carlos_0416.yora.yora.infrastructure.User;
import carlos_0416.yora.yora.infrastructure.YoraApplication;

public class inMemoryAccountService extends BaseinMemoryService {

    public inMemoryAccountService(YoraApplication application){
        super(application);
    }

    @Subscribe
    public void updateProfile(final Account.UpdateProfileRequest request){
        final Account.UpdateProfileResponse response = new Account.UpdateProfileResponse();
        if(request.DisplayName.equals( "carlos")){
            response.setPropertyError("displayName" , "You canot use this name");
        }

        invokeDelayed(new Runnable() {
            @Override
            public void run() {
                User user = application.getAuth().getUser();
                user.setDisplayName(request.DisplayName);
                user.setEmail(request.Email);

                bus.post(response);
                bus.post(new Account.UserDetailsUpdatedEvent(user));

            }
        }, 2000, 3000);

    }



    @Subscribe
    public void updateAvatar(final Account.ChangeAvatarRequest request){
        invokeDelayed(new Runnable() {
            @Override
            public void run() {
                User user = application.getAuth().getUser();
                user.setAvatarUrl(request.NewAvatarUri.toString());

                bus.post(new Account.ChangeAvatarResponse());
                bus.post(new Account.ChangePasswordResponse());
            }
        },4000,5000);
    }

    @Subscribe
    public void changePassword(Account.ChangePasswordRequest request){
        Account.ChangePasswordResponse response = new Account.ChangePasswordResponse();

        if(!request.NewPassword.equals(request.ConfirmNewPassword))
            response.setPropertyError("confimNewPassword","Passwords must match");

        if(request.NewPassword.length()< 3)
            response.setPropertyError("newPassword","Password must be larger then 3 characters");
        postDelayed(response);
    }

    @Subscribe
    public void loginWithUserName(final Account.LoginWithUsernameRequest request){
        invokeDelayed(new Runnable() {
            @Override
            public void run() {
                Account.LoginWithUsernameResponse response = new Account.LoginWithUsernameResponse();


                if (request.Username.equals("carlos"))
                    response.setPropertyError("userName", "Invalid username or password");

                loginUser(new Account.UserResponse());
                bus.post(response);
            }
        }, 1000, 2000);
    }

    @Subscribe
    public void loginWithExternalToken(Account.LoginWithExternalTokenRequest request){
        invokeDelayed(new Runnable() {
            @Override
            public void run() {
                Account.LoginWithExternalTokenResponse response = new Account.LoginWithExternalTokenResponse();
                loginUser(response);
                bus.post(response);
            }
        },1000,2000);
    }


    @Subscribe
    public void register(Account.RegisterRequest request){
        invokeDelayed(new Runnable() {
            @Override
            public void run() {
                Account.RegisterResponse response = new Account.RegisterResponse();
                loginUser(response);
                bus.post(response);

            }
        },1000,2000);
    }

    @Subscribe
    public void externalRegister(Account.RegisterWithExternalTokenRequest request){
        invokeDelayed(new Runnable() {
            @Override
            public void run() {

                Account.RegisterResponse response = new Account.RegisterResponse();
                loginUser(response);
                bus.post(response);

            }
        },1000,2000);
    }


    @Subscribe
    public void loginWithLocalToken(Account.LoginWithLocalTokenRequest request){
        invokeDelayed(new Runnable() {
            @Override
            public void run() {
                Account.LoginWithLocalTokenResponse response = new Account.LoginWithLocalTokenResponse();
                loginUser(response);
                bus.post(response);
            }
        },1000,2000);
    }

    @Subscribe
    public void updateGcmRegistration(Account.UpdateGcmRegistrationRequest request){
        postDelayed(new Account.UpdateGcmRegistrationResponse());
    }


    private void loginUser(Account.UserResponse response){
        Auth auth = application.getAuth();
        User user = auth.getUser();

        user.setDisplayName("Carlos Valentin");
        user.setUserName("Carlos Valentin");
        user.setAvatarUrl("http://www.gravtar.com/avatar/1?d=identicon");
        user.setLoggedIn(true);
        user.setId(123);
        bus.post(new Account.UserDetailsUpdatedEvent(user));

        auth.setAuthToken("fakeauthtoken");

        response.DisplayName = user.getDisplayName();
        response.UserName = user.getUserName();
        response.Email = user.getEmail();
        response.AvatarUrl = user.getAvatarUrl();
        response.id = user.getId();
        response.AuthToken = auth.getAuthToken();
    }
}
