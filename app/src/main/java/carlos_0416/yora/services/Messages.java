package carlos_0416.yora.services;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import carlos_0416.yora.activities.SendMessageActivity;
import carlos_0416.yora.services.entities.Message;
import carlos_0416.yora.services.entities.UserDetails;
import carlos_0416.yora.yora.infrastructure.ServiceResponse;

public final class Messages {
    private Messages() {
    }

    public static class DeleteMessageRequest {
        public int MessageId;

        public DeleteMessageRequest(int messageId) {
            MessageId = messageId;
        }
    }

    public static class DeleteMessageResponse extends ServiceResponse {
        public int MessageId;
    }

    public static class SearchMessagesRequest {
        public int FromContactId;
        public boolean IncludeSentMessages;
        public boolean IncludeReceivedMessages;

        public SearchMessagesRequest(int fromContactId, boolean includeSentMessages, boolean includeReceivedMessages) {
            FromContactId = fromContactId;
            IncludeSentMessages = includeSentMessages;
            IncludeReceivedMessages = includeReceivedMessages;
        }

        public SearchMessagesRequest(boolean includeSentMessages, boolean includeReceivedMessages) {
            FromContactId = -1;
            IncludeSentMessages = includeSentMessages;
            IncludeReceivedMessages = includeReceivedMessages;
        }
    }

    public static class SearchMessagesResponse extends ServiceResponse {
        public List<Message> Messages;
    }

    //What does a message need? Recipient,pathOn local storage, needs our message
    // presisted to the saved state of the messageactivity

    public static class SendMessageRequest implements Parcelable{
        private UserDetails recipent;
        private Uri imagePath;
        private String message;

        public SendMessageRequest(){
        }

        private SendMessageRequest(Parcel in){
            recipent = in.readParcelable(UserDetails.class.getClassLoader());
            imagePath = in.readParcelable(Uri.class.getClassLoader());
            message= in.readString();
        }


        @Override
        public void writeToParcel(Parcel out, int flags) {
            out.writeParcelable(recipent,0);
            out.writeParcelable(imagePath, 0);
            out.writeString(message);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public UserDetails getRecipent() {
            return recipent;
        }

        public void setRecipent(UserDetails recipent) {
            this.recipent = recipent;
        }

        public Uri getImagePath() {
            return imagePath;
        }

        public void setImagePath(Uri imagePath) {
            this.imagePath = imagePath;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public static Creator<SendMessageRequest> CREATOR = new Creator<SendMessageRequest>() {
            @Override
            public SendMessageRequest createFromParcel(Parcel source) {
                return new SendMessageRequest(source);
            }

            @Override
            public SendMessageRequest[] newArray(int size) {
                return new SendMessageRequest[size];
            }
        };
    }

    public static class SendMessageResponse extends ServiceResponse {
        public Message Message;
    }

    public static class MarkMessageAsReadRequest{
        public int MessageId;

        public MarkMessageAsReadRequest(int messageId){
        MessageId= messageId;
        }
    }

    public static class MarkMessageAsReadResponse extends ServiceResponse{
    }

    public static class GetMessageDetailsRequest{
        public int Id;

        public GetMessageDetailsRequest(int id) {
            Id = id;
        }
    }

    public static class GetMessageDetailsResponse extends ServiceResponse{
        public Message Message;
    }
}

