package carlos_0416.yora.activities;

import android.animation.Animator;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.View;

import com.squareup.otto.Bus;

import carlos_0416.yora.R;
import carlos_0416.yora.views.NavDrawer;
import carlos_0416.yora.yora.infrastructure.ActionScheduler;
import carlos_0416.yora.yora.infrastructure.YoraApplication;

public abstract class BaseActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    private boolean isRegisteredwithBus;

    protected YoraApplication application;
    protected Toolbar toolbar;
    protected NavDrawer navDrawer;
    protected boolean isTablet;
    protected Bus bus;
    protected ActionScheduler scheduler;
    protected SwipeRefreshLayout swipeRefresh;

    @Override
    protected void onCreate(Bundle savedState){
        super.onCreate(savedState);
        application = (YoraApplication) getApplication();
        bus = application.getBus();
        scheduler = new ActionScheduler(application);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        isTablet = (metrics.widthPixels/metrics.density) >=600;
        bus.register(this);
        isRegisteredwithBus = true;
    }


    public ActionScheduler getScheduler(){
        return scheduler;
    }

    @Override
    protected void onResume(){
        super.onResume();
        scheduler.onResume();
    }

    @Override
    protected void onPause(){
        super.onPause();
        scheduler.onPause();
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();

        if(isRegisteredwithBus){
            bus.unregister(this);
            isRegisteredwithBus = false;
        }


        if(navDrawer!=null)
            navDrawer.destroy();
    }

    @Override
    public void finish(){
        super.finish();

        if(isRegisteredwithBus){
            bus.unregister(this);
            isRegisteredwithBus =false;
        }
    }

    @Override
    public void setContentView(@LayoutRes int layoutResId){
        super.setContentView(layoutResId);

        toolbar = (Toolbar) findViewById(R.id.include_toolbar);
        if(toolbar!=null) {
            setSupportActionBar(toolbar);
        }

        swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        if(swipeRefresh != null){
            swipeRefresh.setOnRefreshListener(this);
            swipeRefresh.setColorSchemeColors(
                    Color.parseColor("#FF00DDFF"),
                    Color.parseColor("#FF99CC00"),
                    Color.parseColor("#FFFFBB33"),
                    Color.parseColor("#FFFF4444"));
        }
    }


    //Not Required
    public void fadeOut(final FadeOutListener listner){
        View rootView = findViewById(android.R.id.content);
        rootView.animate()
                .alpha(0)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        listner.onFadeOutEnd();

                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .setDuration(300)
                .start();
    }


    protected void setNavDrawer (NavDrawer drawer){
        this.navDrawer = drawer;
        this.navDrawer.create();


        overridePendingTransition(0,0);
        View rootView = findViewById(android.R.id.content);
        rootView.setAlpha(0);
        rootView.animate().alpha(1).setDuration(450).start();
    }

    public Toolbar getToolbar(){
        return toolbar;
    }

    public YoraApplication getYoraApplication(){
        return application;
    }

    @Override
    public void onRefresh() {
    }

    public interface FadeOutListener
    {
        void onFadeOutEnd();
    }


}




